//
// Created by Pulsar on 2019/7/18.
//
#include <QApplication>
#include <CHIP8.h>
#include <QtCore>
#include <Monitor64x32.h>
int main(int argc,char** argv){
    QApplication a(argc, argv);
    CHIP8 chip8;
    chip8.initialize();
//    chip8.loadGame("./Airplane.ch8");
    chip8.loadGame("./Breakout (Brix hack) [David Winter, 1997].ch8");
    Monitor64x32 monitor64X32;
    monitor64X32.link_cpu(chip8);
    monitor64X32.show();
    return a.exec();
}

