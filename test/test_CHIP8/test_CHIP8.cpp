//
// Created by Pulsar on 2019/7/18.
//

#include <iostream>
#include <CHIP8.h>
#include <ATmega328P.h>

int main(int argc, char** argv){
    CHIP8 mcu;
    mcu.initialize();



    ATmega328P arduino;
    mcu.loadGame("./Airplane.ch8");
    std::cout<<"load game file"<<std::endl;
    while(true){
        mcu.runCycle();
    }

    return 0;
}