###########################################################################
#                                                                         #
#                                Find QT                                  #
#                                                                         #
###########################################################################
message(STATUS "CHOOSE Plathform")
if (CMAKE_SYSTEM_NAME MATCHES "Linux")
    MESSAGE(STATUS "current platform: Linux ")
    set(SYSTEM_LIB_DIR /usr/local/libs)
    set(USER_LIB_DIR /usr/lib)
    set(QT5_DIR /home/pulsarv/Qt5.12.3/5.12.3/gcc_64/lib/cmake)

    set(Qt5Widgets_DIR ${QT5_DIR}/Qt5Widgets)
    find_package(Qt5Widgets)
    get_target_property(QtWidgets_location Qt5::Widgets LOCATION)
    message(STATUS "LIB ${QtWidgets_location}")

    set(Qt5Qml_DIR ${QT5_DIR}/Qt5Qml)
    find_package(Qt5Qml)
    get_target_property(QtQml_location Qt5::Qml LOCATION)
    message(STATUS "LIB ${QtQml_location}")


    set(Qt5Xml_DIR ${QT5_DIR}/Qt5Xml)
    find_package(Qt5Xml)
    get_target_property(QtXml_location Qt5::Xml LOCATION)
    message(STATUS "LIB ${QtXml_location}")

    set(Qt5Core_DIR ${QT5_DIR}/Qt5Core)
    find_package(Qt5Core)
    get_target_property(QtCore_location Qt5::Core LOCATION)
    message(STATUS "LIB ${QtCore_location}")

    set(Qt5Gui_DIR ${QT5_DIR}/Qt5Gui)
    find_package(Qt5Gui)
    get_target_property(QtGui_location Qt5::Gui LOCATION)
    message(STATUS "LIB ${QtGui_location}")


    set(Qt5OpenGL_DIR ${QT5_DIR}/Qt5OpenGL)
    find_package(Qt5OpenGL)
    get_target_property(QtOpenGL_location Qt5::OpenGL LOCATION)
    message(STATUS "LIB ${QtOpenGL_location}")

    set(Qt5Network_DIR ${QT5_DIR}/Qt5Network)
    find_package(Qt5Network)
    get_target_property(QtNetwork_location Qt5::Network LOCATION)
    message(STATUS "LIB ${QtNetwork_location}")

    set(CMAKE_AUTOUIC ON)

elseif (CMAKE_SYSTEM_NAME MATCHES "Windows")
    set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++")
    MESSAGE(STATUS "current platform: Windows")
    set(QT5_DIR D:/Qt/Qt5.9.3/5.9.3/mingw53_32/lib/cmake)
    find_package(Qt5 COMPONENTS Core Widgets Qml OpenGL  REQUIRED)

elseif (CMAKE_SYSTEM_NAME MATCHES "FreeBSD")
    MESSAGE(STATUS "current platform: FreeBSD")
endif ()
set(CMAKE_INCLUDE_CURRENT_DIR ON)



