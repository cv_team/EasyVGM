###########################################################################
#                                                                         #
#                             Find Boost                                  #
#                                                                         #
###########################################################################
#set(Boost_USE_STATIC_LIBS OFF) # only find static libs
#set(Boost_USE_MULTITHREADED ON)
#set(Boost_USE_STATIC_RUNTIME ON)
#find_package(Boost COMPONENTS date_time filesystem  system REQUIRED)
#if (Boost_FOUND)
#    include_directories(${Boost_INCLUDE_DIRS})
#    link_directories(${Boost_LIBRARYDIR})
#    MESSAGE(STATUS "Boost_INCLUDE_DIRS = ${Boost_INCLUDE_DIRS}.")
#    MESSAGE(STATUS "Boost_LIBRARIES = ${Boost_LIBRARIES}.")
#    MESSAGE(STATUS "Boost_LIB_VERSION = ${Boost_LIB_VERSION}.")
#endif ()

