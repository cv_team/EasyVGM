//
// Created by pulsarv on 19-7-24.
//

#include <NESEmulator.h>
#include <cstring>

void NESEmulator::tick() {
}

void NESEmulator::draw_screen(uint8_t x, uint8_t y, uint8_t n) {

}

void NESEmulator::printState() {

}

void NESEmulator::initialize() {
    int i;

    PC = 0x200;
    opcode = 0;
    I = 0;
    SP = 0;

    memset(memory, 0, sizeof(uint8_t) * NES_MEM_SIZE);
    memset(V, 0, sizeof(uint8_t) * 16);
    memset(gfx, 0, sizeof(uint8_t) * NES_GFX_SIZE);
    memset(stack, 0, sizeof(uint16_t) * NES_STACK_SIZE);
    memset(key, 0, sizeof(uint8_t) * NES_KEY_SIZE);


    draw_flag = true;
    delay_timer = 0;
    sound_timer = 0;
    srand(time(NULL));
}

void NESEmulator::print_cpu_info() {
}

void NESEmulator::loadGame(char *file_path) {

}

void NESEmulator::debug_screen() {

}

void NESEmulator::setkeys(int index, int state) {

}

unsigned short NESEmulator::get_opcode() {
    return memory[PC] << 8 | memory[PC + 1];
}

