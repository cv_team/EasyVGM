//
// Created by Pulsar on 2019/7/18.
//

#ifndef EASYMVM_ATMEGA328P_H
#define EASYMVM_ATMEGA328P_H

#include <base_vm_env/base_vm_env.hpp>
class MSP430F55529 : public base_vm_env {
public:
    const char* name="MSP430F5529";
};


#endif //EASYMVM_ATMEGA328P_H
