//
// Created by Pulsar on 2019/7/18.
//

#ifndef EASYMVM_POWER_H
#define EASYMVM_POWER_H

#include <base_vm_obj/base_vm_obj.hpp>
class Power: public BaseVMObject {
public:
    double vcc;//正极
    double gnd;//负极
    Power(int volate=3.3);
};


#endif //EASYMVM_POWER_H
