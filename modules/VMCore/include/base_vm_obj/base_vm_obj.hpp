//
// Created by Pulsar on 2019/7/18.
//

#ifndef EASYMVM_BASE_VM_OBJ_H
#define EASYMVM_BASE_VM_OBJ_H

#include <base_vm_obj/object_type.h>
class BaseVMObject {
public:
    bool state;//接通状态
    double resistance;//电阻
    double inductance;//电感
    double volate_in;//输入电压
    double current_in;//输入电流
    double response_voltage;//逻辑响应电压
    double volate_out;//输出电压
    double current_out;//输出电流
    double volate_max;//最大电压
    double volate_min;//最小电压
    double current_max;//最大电流
    double current_min;//最小电流
};
#endif //EASYMVM_BASE_VM_OBJ_H
