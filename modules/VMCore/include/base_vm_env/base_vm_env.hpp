//
// Created by Pulsar on 2019/7/18.
//

#ifndef EASYMVM_BASE_VM_ENV_H
#define EASYMVM_BASE_VM_ENV_H

#include <iostream>

class base_vm_env {
public:
    char *name;
    int screen_rows;
    int screen_cols;
    int clock_rate_ms;
    int clock_hz;
    int pixel_size;
    int draw_flag;

    virtual void initialize() {};

    virtual void runCycle() {};

    virtual void powerON() {};

    virtual void powerOFF() {};

    virtual void print_cpu_info() {};

    virtual void tick(){};

    virtual void printState(){};
};


#endif //EASYMVM_BASE_VM_ENV_H
