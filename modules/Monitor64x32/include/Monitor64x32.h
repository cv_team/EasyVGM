//
// Created by Pulsar on 2019/7/18.
//

#ifndef EASYMVM_MONITOR16X16_H
#define EASYMVM_MONITOR16X16_H

#include <sys/time.h>
#include <iostream>
#include <QtWidgets>
#include <QMainWindow>
#include <QtCore>
#include <QTimer>
#include <modules/VMCore/include/base_vm_env/base_vm_env.hpp>
#include <CHIP8.h>
#include "Monitor64x32GL.h"
#include "ui_Monitor64x32.h"

namespace Ui {
    class Monitor64x32;
}
class Monitor64x32 : public QMainWindow {
Q_OBJECT
public:
    struct timeval clock_prev;

    Monitor64x32(QWidget *parent = 0);

    void link_cpu(CHIP8 &chip8);
    int keymap(unsigned char k);

    void keyPressEvent(QKeyEvent *event);


    int timediff_ms(struct timeval *end, struct timeval *start);


    void keyReleaseEvent(QKeyEvent *event);


    ~Monitor64x32();

private  slots:

    void paint_screen();


    void timeout_sc();

signals:
    void draw_pixel(int row, int col, float color);
    void repaint_screen();

private:
    CHIP8 cpu;
    Monitor64x32GL *glwindow;
    void paint_pixel(int row, int col, unsigned char color);
    void paint_cell(int row, int col, unsigned char color);

};

#endif //EASYMVM_MONITOR16X16_H
