//
// Created by Pulsar on 2019/7/18.
//

#ifndef EASYMVM_MONITOR64X32GL_H
#define EASYMVM_MONITOR64X32GL_H

#include <GL/gl.h>
#include <GL/glu.h>
#include<QOpenGLWidget>
#include <QWidget>
#include <QtCore/qglobal.h>


class   Monitor64x32GL : public QOpenGLWidget {
    Q_OBJECT
public:
    Monitor64x32GL(QWidget *parent = nullptr);

    void initializeGL();



    void paint_cell(int row, int col, unsigned char color);


    void resizeGL(int width, int height);

    virtual ~Monitor64x32GL();

signals:
    void paintGL();


public slots:
    void paint_pixel(int row, int col, float color);


private:
    int PIXEL_SIZE_SC;//像素大小
    int SCREEN_ROWS_SC;//屏幕行数
    int SCREEN_COLS_SC;//屏幕列数
};
#endif //EASYMVM_MONITOR64X32_GL_H
