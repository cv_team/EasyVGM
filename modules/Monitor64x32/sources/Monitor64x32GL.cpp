//
// Created by Pulsar on 2019/7/18.
//

#ifndef EASYMVM_MONITOR64X32_GL_H
#define EASYMVM_MONITOR64X32_GL_H

#include <GL/gl.h>
#include <GL/glu.h>
#include<QOpenGLWidget>
#include <iostream>
#include <Monitor64x32GL.h>

#endif //EASYMVM_MONITOR64X32_GL_H

Monitor64x32GL::Monitor64x32GL(QWidget *parent) : QOpenGLWidget(parent) {

}

void Monitor64x32GL::initializeGL() {
    glClearColor(0.0, 0.0, 0.0, 0.0);
}



void Monitor64x32GL::resizeGL(int width, int height) {
//    glViewport(0,0,(GLint)width,(GLint)height);
//    glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
    gluPerspective(45.0, (GLfloat) width / (GLfloat) height, 0.1, 100);
//    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}


void Monitor64x32GL::paint_pixel(int row, int col, float color) {
//    std::cout<<"PAINT"<<std::endl;
    float screen_row_width = 1.0 / 64.0 * 2;
    float screen_col_width = 1.0 / 32.0 * 2;
    glColor3f(color / 255.0, color / 255.0, color / 255.0);
    glBegin(GL_QUADS);
    glVertex2f(screen_row_width * col, -screen_col_width * row);
    glVertex2f(screen_row_width * col, -screen_col_width * (row + 1.0));
    glVertex2f(screen_row_width * (col + 1.0), -screen_col_width * (row + 1.0));
    glVertex2f(screen_row_width * (col + 1.0), -screen_col_width * row);
    glEnd();
}

void Monitor64x32GL::paint_cell(int row, int col, unsigned char color) {
    int pixel_row = row * PIXEL_SIZE_SC;
    int pixel_col = col * PIXEL_SIZE_SC;
    int drow, dcol;

//    for (drow = 0; drow < PIXEL_SIZE_SC; drow++) {
//        for (dcol = 0; dcol < PIXEL_SIZE_SC; dcol++) {
//            paint_pixel(pixel_row + drow, pixel_col + dcol, color);
//        }
//    }
}

Monitor64x32GL::~Monitor64x32GL() {

}
